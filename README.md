# TEZ_theGame

### Auteur : 
    Zoé Chevallier 
    Etienne Guillou
    Thibault Denis

Nous avons prévu de coder notre projet en Java, avec une interface en JEE. 

Nous voulons réaliser un jeu, qui reprend les principes de Cookie Clicker et de Candy Box. 

Le but du jeu sera “d’attirer des développeurs” en “minant du bitcoin” de la même façon qu’on cuisine des cookies dans CookieClicker. Cela permettra au joueur de développer une entreprise de jeux vidéo, de plus en plus importante. Ainsi, on pourra débloquer des nouveaux “mini-jeux de plateau”, que nous allons implémenter. 
Avec un tel principe, nous aurons un projet modulable. Cela nous permettra de rendre notre code générique et évolutif. 

### Que réaliser ? 
Le jeu se veut être un jeu de simulation de gestion d'entreprise. Nous aurons à débloquer des mines et des employés, qui seront gérés par des managers. Les managers auront des caractéristiques spéciales qui interagiront avec les employés (motivation, performance...). Le but sera d'optimiser au mieux la production afin de développer au mieux l'entreprise. De plus, lors de l'évolution de l'entreprise, des mini jeux se débloqueront et pourront être joués par le joueur (tel que Tic Tac Toe ou Puissance 4). Ceux-ci permettront de gagner ou perdre des employés.  
  
Pour cela nous allons créer un projet avec un serveur en Java et un client web en JEE. Nous aurons donc une interface web permettant de gérer les différents composants instanciés dans le jeu. Nous aurons également des DAO qui permettront de gérer les échanges entre le client et le serveur de façon normalisé.   


### Comment le réaliser ? 
[Diagramme de classe]

### Date : 
    28/SEPT/18
### Etat : 
    En développement