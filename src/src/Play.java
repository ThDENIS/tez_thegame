package src;

/**
* Classe permettant faisant office de serveur pour les jeux.
* Il permet de créer un jeu lorsqu'on entre sur une page, et d'appeler les fonctions des classes de Jeu à utiliser 
*/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/connected/Play")
public class Play extends HttpServlet {
	
	private TicTacToe tictactoe;
	private Puissance4 puissance4; 
	
	/**
	* Fonctions pour la méthode "POST", appelées par la javascript (fichiers JS correspondants aux jeux)
	*/
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");

		//On lance la page "tictactoe.jsp", on veut donc créer un nouvel objet TicTacToe
		if(action.equals("newgame")) {
			createNewGame("tictactoe");
		}

		//On lance la page "p4.jsp", on veut donc créer un nouvel objet Puissance 4
		else if (action.equals("newgameP4")){
			createNewGame("puissance4");
		}

		//Lorsqu'on joue au TicTacToe, et que l'utilisateur appuie sur le jeu pour placer une pièce 
		else if(action.equals("choosecase")) {
			String i = request.getParameter("i");
			String j = request.getParameter("j");
			
			chooseCase(Integer.parseInt(i), Integer.parseInt(j));
			response.setContentType("text/plain");
			
			int winner = tictactoe.isOver(); 
			
			if (winner == 0) { //Vérifie si le jeu est termine ou pas
				response.getWriter().write(tictactoe.displayPlateau());
				
			}
			else { 
				//Si le jeu est terminé, on affiche le plateau qui ne permet plus aux utilisateurs de jouer
				//On affiche donc le gagnant, et le match nul
				response.getWriter().write(tictactoe.displayWinner(winner));
			}
		}

		//Lorsqu'on joue au Puissance 4, et que l'utilisateur appuie sur le jeu pour placer une pièce
		else if(action.equals("choosecaseP4")) {
			String j = request.getParameter("j");
			
			chooseCase(Integer.parseInt(j));
			response.setContentType("text/plain");
			
			int winner = puissance4.finPartie(); 
			if (winner == 0) //Verifie si le jeu est termine ou pas			
				response.getWriter().write(puissance4.displayPlateau());
				
			else 
				//Si le jeu est terminé, on affiche le plateau qui ne permet plus aux utilisateurs de jouer
				//On affiche donc le gagnant, et le match nul
				response.getWriter().write(puissance4.displayWinner(winner));
			
			
		}
	
		
	}
	
	//Fonction qui crée un jeu : TicTacToe ou Puissance4 suivant le cas
	private void createNewGame(String jeu) {
		switch (jeu) {
			case "tictactoe" : 
				tictactoe = new TicTacToe();
				break; 
			case "puissance4" : 
				puissance4 = new Puissance4(); 
				break; 
		}
		
	}

	//Fonction qui place la case dans le plateau, et qui fait jouer l'adversaire automatiquement (Pour le TicTacToe)
	private void chooseCase(int i, int j) {
		if (tictactoe.plateau[i][j] == 0) {
			tictactoe.plateau[i][j] = tictactoe.getTourJeu();
			tictactoe.changeTourJeu(tictactoe.getTourJeu());
			if (tictactoe.isOver() == 0) {
				Coords c = tictactoe.niveauFacile();
				tictactoe.plateau[c.getX()][c.getY()] = tictactoe.getTourJeu();
				tictactoe.changeTourJeu(tictactoe.getTourJeu());
			}
			
		}
	}
	
	//Fonction qui place la case dans le plateau, et qui fait jouer l'adversaire automatiquement (Pour le Puissance4)
	private void chooseCase(int j) {
		Coords c = puissance4.getCaseToPose(j); 
		
		if (c != null) {
			puissance4.plateau[c.getX()][c.getY()] = puissance4.getTourJeu(); 
			puissance4.changeTourJeu(puissance4.getTourJeu());
			if (puissance4.finPartie() == 0) {
				Coords coords = puissance4.adversaireFacile();
				puissance4.plateau[coords.getX()][coords.getY()] = puissance4.getTourJeu();
				puissance4.changeTourJeu(puissance4.getTourJeu());
			}
		}

	}
}
