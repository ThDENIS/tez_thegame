package src;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;


/**
* Classe qui fait les foncitons nécessaires pour jouer au Puissance 4
*/

public class Puissance4 extends Jeu implements Serializable {

	private static final long serialVersionUID = 1L;
	private int tour_jeu; 
	private int objectif; 
	
	public Puissance4() {
		super(6, 7);
		// TODO Auto-generated constructor stub
		tour_jeu = 1; 
		objectif = 4;
		
	}
	
	public int getTourJeu() {
		return tour_jeu;
	}
	
	public void changeTourJeu(int tour) {
		tour_jeu = tour_jeu == 1 ? 2 : 1;
	}
	
	/**
	* Fonction qui permet d'afficher le plateau. 
	* Dans le onClick, on lance la fonction javascript présente dans le fichier p4.js
	*/

	public String displayPlateau() {
		String s = ""; 
		
		String pion = "";
		for (int i=0; i<this.largeur_plateau; i++) {
			for (int j=0; j<this.longueur_plateau; j++) {

				switch (this.plateau[i][j]) {
					case 0 : 
						s += "<img class='p4' src='../images/vide.png' onClick='chooseCase(" + j + ")'> ";
						break;
					case 1 : 
						s += "<img class='p4' src='../images/rond.png' onClick='chooseCase("+ j + ")'> ";
						break;
					case 2 : 
						s += "<img class='p4' src='../images/rondBleu.png' onClick='chooseCase("+ j + ")'> ";
						break;
				}
				
			}
			s += "<br/>";
		}
		
		return s; 
	}
	
	/**
	* Fonction qui permet de savoir sur quelle case on va mettre le pion.
	* En effet, pour le puissance 4 on choisit une colonne, et on place le pion au plus bas sur cette colonne
	*/
	public Coords getCaseToPose(int j) {
		for (int i=1; i<=this.largeur_plateau; i++) {
			if (this.plateau[this.largeur_plateau-i][j] == 0) 
				return new Coords(this.largeur_plateau-i,j); 
		}
		return null; 
	}
	
	/**
	* Fonction qui vérifie si une partie est terminée ou pas. On a procédé ici par étape (vérification pour les colonnes, puis les lignes puis les diagonales)
	*/
	public int finPartie() {
		int colonne, ligne, diagonale; 
		//Regarde pour les colonnes
		for (int j=0; j<longueur_plateau; j++) {
			colonne = colonne(j); 
			if (colonne != 0)
				return colonne; 				
		}
		for (int i=0; i<largeur_plateau; i++) {
			ligne = ligne(i); 
			if (ligne != 0)
				return ligne; 				
		}
		for (int i=0; i<largeur_plateau; i++) {
			diagonale = diagonale(i); 
			if (diagonale != 0)
				return diagonale;
			diagonale = diagonaleInverse(i); 
			if (diagonale != 0)
				return diagonale;
		}
		return completed(); 
	}

	/**
	* Fonction qui vérifie si une grille est remplie (et donc qu'il y a match nul)
	*/ 
	public int completed() {
		for (int i=0; i<this.largeur_plateau;i++) {
			for (int j=0; j<this.longueur_plateau; j++) {
				if (this.plateau[i][j] == 0)
					return 0; 
			}
		}
		return -1;
	}
	
	/**
	* Fonction qui vérifie s'il y a une suite de 4 pièces de la même couleur sur une colonne donnée
	*/ 
	public int colonne(int j) {
		int cpt1 = 0; int cpt2 = 0; 
		for (int i=0; i<largeur_plateau; i++) {
			if (this.plateau[i][j] == 1) {
				cpt1++; cpt2 = 0; 
				if (cpt1 >= objectif) {
					return 1; 
				}
			}
			else if (this.plateau[i][j] == 2) {
				cpt2++; cpt1 = 0; 
				if (cpt2 >= objectif ) {
					return 2; 
				}
			}
			else {
				cpt1 = 0; cpt2 = 0; 
			}
		}
		
		
		return 0; 
	}
	
	/**
	* Fonction qui vérifie s'il y a une suite de 4 pièces de la même couleur sur une ligne donnée
	*/ 
	public int ligne(int i) {
		int cpt1 = 0; int cpt2 = 0; 
		for (int j=0; j<longueur_plateau; j++) {
			if (this.plateau[i][j] == 1) {
				cpt1++; cpt2 = 0; 
				if (cpt1 >= objectif )
					return 1;
			}
			else if (this.plateau[i][j] == 2) {
				cpt2++; cpt1 = 0; 
				if (cpt2 >= objectif )
					return 2; 
			}
			else {
				cpt1 = 0; cpt2 = 0; 
			}
		}
		return 0; 
	}
	
	/**
	* Fonction qui vérifie s'il y a une suite de 4 pièces de la même couleur sur une diagonale donnée
	*/ 
	public int diagonale(int num) {
		int cpt1 = 0; int cpt2 = 0; 
		int i= positionDep(num).getX(); int j= positionDep(num).getY();
		int k=0; 
		
		while (i+k<largeur_plateau && j+k<longueur_plateau) {
			if (this.plateau[i+k][j+k] == 1) {
				cpt1++; cpt2 = 0; 
				if (cpt1 >= objectif )
					return 1;
			}
			else if (this.plateau[i+k][j+k] == 2) {
				cpt2++; cpt1 = 0; 
				if (cpt2 >= objectif )
					return 2; 
			}
			else {
				cpt1 = 0; cpt2 = 0; 
			}
			k++; 
		}
		return 0; 
	}

	/**
	* Fonction qui vérifie s'il y a une suite de 4 pièces de la même couleur sur une diagonale inverse donnée
	*/ 
	public int diagonaleInverse(int num) {
		int cpt1 = 0; int cpt2 = 0; 
		int i= positionDepInverse(num).getX(); int j= positionDepInverse(num).getY();
		int k=0; 
		
		while (i+k<largeur_plateau && j-k>=0) {
			if (this.plateau[i+k][j-k] == 1) {
				cpt1++; cpt2 = 0; 
				if (cpt1 >= objectif )
					return 1;
			}
			else if (this.plateau[i+k][j-k] == 2) {
				cpt2++; cpt1 = 0; 
				if (cpt2 >= objectif )
					return 2; 
			}
			else {
				cpt1 = 0; cpt2 = 0; 
			}
			k++; 
		}
		return 0; 
	}
	
	/**
	* Fonction qui affiche le plateau du jeu terminé et qui affiche le gagnant
	*/
	public String displayWinner(int winner) {
		String s = ""; 
		
		String pion = "";
		for (int i=0; i<this.largeur_plateau; i++) {
			for (int j=0; j<this.longueur_plateau; j++) {

				switch (this.plateau[i][j]) {
					case 0 : 
						s += "<img class='p4' src='../images/vide.png'> ";
						break;
					case 1 : 
						s += "<img class='p4' src='../images/rond.png'> ";
						break;
					case 2 : 
						s += "<img class='p4' src='../images/rondBleu.png'> ";
						break;
				}
			}
			s += "<br/>";
		}
		s += "<br/>";
		if (winner == -1)
			s += "Match nul !!! ";
		else
			s += "PLAYER " + winner + "  YOU WIN !! ";
		return s; 
	}
	
	/**
	* Fonction qui permet d'avoir la position de la première case de la diagonale
	*/
	public Coords positionDep(int i) {
		switch (i) {
			case 0 : return new Coords(2,0); 
			case 1 : return new Coords(1,0); 
			case 2 : return new Coords(0,0); 
			case 3 : return new Coords(0,1); 
			case 4 : return new Coords(0,2); 
			case 5 : return new Coords(0,3); 
		}
		return null; 
	}
	
	/**
	* Fonction qui permet d'avoir la position de la première case de la diagonale inverse
	*/
	public Coords positionDepInverse(int i) {
		switch (i) {
			case 0 : return new Coords(0,longueur_plateau-4); 
			case 1 : return new Coords(0,longueur_plateau-3); 
			case 2 : return new Coords(0,longueur_plateau-2); 
			case 3 : return new Coords(0,longueur_plateau-1); 
			case 4 : return new Coords(1,longueur_plateau-1); 
			case 5 : return new Coords(2,longueur_plateau-1); 
		}
		return null; 
	}
	
	/**
	* Fonction qui renvoie toutes les cases libres pour placer un pion
	*/
	public ArrayList<Coords> coupsPossibles(){
		ArrayList<Coords> coups = new ArrayList<Coords>(); 
		for (int j=0; j<longueur_plateau; j++) {
			Coords c = getCaseToPose(j);
			if (c != null)
				coups.add(c); 
		}
		return coups; 
	}

	
	/**
	* Fonction qui permet d'avoir un ennemi, assez facile a battre. 
	* Il choisit une case au hasard parmis les coups possibles
	*/
	public Coords adversaireFacile() {
		
		ArrayList<Coords> cp = new ArrayList<Coords>(coupsPossibles()); 

		int random = (int)(Math.random() * (cp.size()));
		
		return cp.get(random);
	}
}