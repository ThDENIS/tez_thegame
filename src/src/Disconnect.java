package src;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns="/Disconnect")
public class Disconnect extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * M�thode appel� lors de la d�connection d'un user.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		boolean disconnect = true;
		if(request.getParameter("disconnect") == null)
			disconnect = false;
		RequestDispatcher disp;

		if (disconnect == true){
			Entreprise e = (Entreprise) session.getAttribute("entreprise");
			e.stopThread();
			try { // Si le dossier de sauvegarde n'existe pas, on le cr��
			 	File file = new File(StorageLocation.getStorageLocation(0), StorageLocation.getStorageLocation(1));
			 	if (file.exists() && file.isDirectory()) {
			 		System.out.println("exist d�ja");
			 	}else{
			 		File f1 = file; 
			 		f1.mkdir();
			 	}
			 	file = new File(StorageLocation.getStorageLocation(0) + StorageLocation.getStorageLocation(1)+ StorageLocation.getStorageLocation(2));
			 	if (file.exists() && file.isDirectory()) {
			 		System.out.println("exist d�ja");
			 	}else{
			 		File f1 = file; 
			 		f1.mkdir();
			 	}
			 	
				FileOutputStream fos = new FileOutputStream(StorageLocation.getStorageLocation(0) + StorageLocation.getStorageLocation(1)+ StorageLocation.getStorageLocation(2)+"\\"+e.getIdEntreprise()+".txt");
	            ObjectOutputStream oos = new ObjectOutputStream(fos);
	            oos.writeObject(e); // stock l'entreprise dans le dossier
	            oos.close();
	            fos.close();
				Dao.disconnect();
			} catch (SQLException e1) {
				//e1.printStackTrace();
			}
			session.setAttribute("entreprise", null);
			session.setAttribute("authenticated", "non");
			response.sendRedirect( request.getContextPath() + "/home.html");
		} else {
			response.sendRedirect( request.getContextPath() + "/connected/ok.jsp");
		}
	}
}
