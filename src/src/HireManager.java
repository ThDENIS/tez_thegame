package src;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/HireManager")
public class HireManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * M�thode appel�e lors que l'ajout d'un manager.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		Entreprise e = (Entreprise) session.getAttribute("entreprise");
		Integer idMine = Integer.parseInt(request.getParameter("idMine"));
		String managerName = request.getParameter("manager");
		RequestDispatcher disp;
		boolean go = true;
		for (Integer i = 0; i < e.getMineAvailable(); i++)
			if (e.getMine(i).getEquipe() != null && e.getMine(i).getEquipe().getManager().getNom().equals(managerName))
				go = false;
		if (go) {
			e.getMine(idMine).hireTeam(e.getEquipe(managerName));
			response.sendRedirect(request.getContextPath() + "/connected/mines.jsp");
		} else {
			response.sendRedirect(request.getContextPath() + "/connected/fail.jsp");

		}

	}
}
