package src;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

public class LoginFilter implements Filter {
	private ServletContext ctx;

	public void init(FilterConfig cfg) throws ServletException {
		ctx = cfg.getServletContext();
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession();
		String auth = (String) session.getAttribute("authenticated");
		if ("yes".equals(auth))
			chain.doFilter(request, response);
		else
			httpResponse.sendRedirect(ctx.getContextPath() + "/home.html");
	}
}
