package src;

import java.io.Serializable;

public class Equipe implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer nbEmployes = 0;
	private Integer fatigue = 0;
	private Manager manager = null;
	
	public Equipe() {
		this(0,0,null);
	}
	
	public Equipe(Integer nbEmployes, Integer fatigue) {
		super();
		this.nbEmployes = nbEmployes;
		this.fatigue = fatigue;
		this.manager = new Manager();
	}
	
	public Equipe(Integer nbEmployes, Integer fatigue, Manager manager) {
		super();
		this.nbEmployes = nbEmployes;
		this.fatigue = fatigue;
		this.manager = manager;
	}
	public Integer getNbEmployes() {
		return nbEmployes;
	}
	public void setNbEmployes(Integer nbEmployes) {
		this.nbEmployes = nbEmployes;
	}
	public void hireEmployees(Integer quantity) throws Exception {
		if(this.manager.getNbMaxEmployees() >= this.nbEmployes+quantity) 
			this.nbEmployes += quantity;
		else 
			throw new Exception("Ce manager ne peux pas avoir autant d'employee");
			
	}
	public void fireEmployees(Integer quantity) throws Exception {
		if(this.nbEmployes >= quantity)
			this.nbEmployes -= quantity;
		else
			throw new Exception("Vous ne pouvez pas virer plus d'employ�e qu'il y en a dans l'�quipe");
	}
	public Integer getFatigue() {
		return fatigue;
	}
	public void setFatigue(Integer fatigue) {
		this.fatigue = fatigue;
	}
	
	public Manager getManager() {
		return manager;
	}
	
}
