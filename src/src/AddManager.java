package src;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/AddManager")
public class AddManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * M�thode appel�e lors que l'ajout d'un manager.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		Entreprise e = (Entreprise) session.getAttribute("entreprise");
		RequestDispatcher disp;
		try {
			if (Dao.addManager(e)) {
				response.sendRedirect(request.getContextPath() + "/connected/shop.jsp");
			} else {
				response.sendRedirect(request.getContextPath() + "/connected/fail.jsp");

			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}
}
