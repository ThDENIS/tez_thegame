package src;

import java.io.Serializable;
import java.util.ArrayList;

public class Manager implements Serializable{

	private static final long serialVersionUID = 1L;
	private String nom = null;
	private Integer nbMaxEmployees = null;
	private ArrayList<Boost> boosts = null;
	
	
	public Manager() {
		this("noname",0); // attention Noname sert pour le check de l'�tat des mines.
	}
	
	public Manager(String nom, Integer nbMaxEmployees) {
		super();
		this.nom = nom;
		this.nbMaxEmployees = nbMaxEmployees;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Integer getNbMaxEmployees() {
		return nbMaxEmployees;
	}
	public void setNbMaxEmployees(Integer nbMaxEmployees) {
		this.nbMaxEmployees = nbMaxEmployees;
	}
	
	public Boost getBoost(Integer i) {
		return boosts.get(i);
	}
	public void addBoost(Boost b) {
		boosts.add(b);
	}
	public Integer getNbBoosts() {
		return boosts.size();
	}
	
}
