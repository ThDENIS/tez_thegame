package src;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;

import javax.net.ssl.HandshakeCompletedListener;

public final class Dao {

	static private Connection conn = null;

	/**
	 * M�thode permettant de se connecter � la base de donn�es
	 * 
	 * @throws SQLException
	 */
	static private void openConn() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Pilot not found!");
		}
		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/bitcoin_quest", "root", "");
	}

	/**
	 * M�thode poru se d�connecter de la base de donn�es.
	 * 
	 * @throws SQLException
	 */
	static private void closeConn() throws SQLException {
		conn.close();
	}

	/**
	 * M�thode poour supprimer un user
	 * 
	 * @param idUser
	 * @throws SQLException
	 */
	static private void deleteUser(final String idUser) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("DELETE FROM users WHERE IdUser = " + idUser.toString() + "");
		int rs = ps.executeUpdate();
		ps.close();
	}

	/**
	 * M�thode pour ajouter un compte dans la base de donn�es.
	 * 
	 * @param login
	 * @param hash
	 * @param isAdmin
	 * @return
	 * @throws SQLException
	 */
	static public String addAccount(final String login, final String hash, final boolean isAdmin) throws SQLException {
		String id = null;
		PreparedStatement ps = conn.prepareStatement(
				"INSERT INTO users (Login, Hash, IsAdmin) VALUES ( '" + login + "', '" + hash + "', " + isAdmin + ")");
		int rs = ps.executeUpdate();
		ps = conn.prepareStatement("SELECT IdUser FROM users WHERE Login = '" + login + "' AND Hash = '" + hash + "';");
		ResultSet res = ps.executeQuery();
		res.next();
		try {
			id = res.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ps.close();
		return id;
	}

	/**
	 * M�thode pour modifier un compte
	 * 
	 * @param login
	 * @param hash
	 * @param isAdmin
	 * @param idUser
	 * @throws SQLException
	 */
	static public void modifyAccount(final String login, final String hash, final boolean isAdmin, final String idUser)
			throws SQLException {

		PreparedStatement ps;

		ps = conn.prepareStatement("UPDATE users " + "SET Login = '" + login + "', Hash = '" + hash + "',"
				+ "IsAdmin = " + isAdmin + " WHERE IdUser = " + idUser + ";");
		int rs = ps.executeUpdate();
		ps.close();
	}

	/**
	 * M�thode pour connecter un user si il existe dans la base de donn�es
	 * 
	 * @param login
	 * @param hash
	 * @return
	 * @throws SQLException
	 */
	static public String connect(final String login, final String hash) throws SQLException {
		String id = "-1";
		openConn();
		PreparedStatement ps = conn
				.prepareStatement("SELECT IdUser FROM users WHERE Login = '" + login + "' AND Hash = '" + hash + "';");
		ResultSet res = ps.executeQuery();
		res.next();
		try {
			id = res.getString(1);
			System.out.println(id);
		} catch (SQLException e) {
			closeConn();
			e.printStackTrace();
			return id;
		}
		return id;
	}

	/**
	 * M�thode qui appel la m�thode pour se d�connecter de la base de donn�es.
	 * 
	 * @throws SQLException
	 */
	static public void disconnect() throws SQLException {
		closeConn();
	}

	/**
	 * M�thode qui retourne le score du user pass� en param�tre
	 * 
	 * @param idUser
	 * @return
	 * @throws SQLException
	 */
	static public double getScore(final String idUser) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT Score FROM users WHERE IdUser = '" + idUser + "';");
		ResultSet res = ps.executeQuery();
		res.next();
		Double score;
		try {
			score = res.getDouble(1);
			System.out.println(score);
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
		return score;
	}

	/**
	 * M�thode pour modifier le score du user pass� en param�tre dans la BBD.
	 * 
	 * @param idUser
	 * @param score
	 * @throws SQLException
	 */
	static public void modifyScore(final String idUser, final Double score) throws SQLException {
		PreparedStatement ps;

		ps = conn.prepareStatement("UPDATE users " + "SET Score = '" + score + "' WHERE IdUser = " + idUser + ";");
		System.out.println("thread score dans le DAO : " + score);
		int rs = ps.executeUpdate();
		ps.close();
	}

	/**
	 * M�thode qui retourne le co�t d'un Employee dans la BDD.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static int getEmployeeCost() throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT price FROM shop WHERE name = 'employee';");
		ResultSet res = ps.executeQuery();
		res.next();
		return res.getInt(1);
	}

	/**
	 * M�thode qui retourne le co�t d'un manager dans la BDD.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static int getManagerCost() throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT price FROM shop WHERE name = 'manager';");
		ResultSet res = ps.executeQuery();
		res.next();
		return res.getInt(1);
	}

	/**
	 * M�thode qui retourne le co�t d'une mine dans la BDD.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static int getMineCost() throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT price FROM shop WHERE name = 'mine';");
		ResultSet res = ps.executeQuery();
		res.next();
		return res.getInt(1);
	}

	/**
	 * M�thode qui ajouter X employ�es dans l'entreprise donn�e.
	 * 
	 * @param e
	 * @param quantities
	 * @return
	 * @throws SQLException
	 */
	public static boolean addEmployee(Entreprise e, Integer quantities) throws SQLException {
		String idUser = e.getIdEntreprise();
		Double coins = e.getPoints();
		boolean bool = false;
		try {
			Integer price = Dao.getEmployeeCost();
			if (price * quantities <= coins) {
				Dao.modifyScore(idUser, (coins - (price * quantities)));
				e.setPoints(coins - (price * quantities));
				e.setNbEmployeeDisponible(e.getNbEmployeeDisponible() + quantities);
				bool = true;
			} else {
				System.out.println("addEmployee fail");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return bool;
	}

	/**
	 * M�thode qui ajoute une mine � l'entreprise donn�e.
	 * 
	 * @param e
	 * @return
	 * @throws SQLException
	 */
	public static boolean addMine(Entreprise e) throws SQLException {
		String idUser = e.getIdEntreprise();
		Double coins = e.getPoints();
		boolean bool = false;
		try {
			Integer price = Dao.getMineCost();
			if (price <= coins) {
				Dao.modifyScore(idUser, (coins - price));
				e.setPoints(coins - price);
				e.addMine();
				bool = true;
			} else {
				System.out.println("addMine fail");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return bool;
	}

	/**
	 * M�thode qui ajoute un manager pour une entreprise donn�e, retourne false si
	 * aucune mine est disponible.
	 * 
	 * @param e
	 * @return
	 * @throws SQLException
	 */
	public static boolean addManager(Entreprise e) throws SQLException {
		String idUser = e.getIdEntreprise();
		Double coins = e.getPoints();
		boolean bool = false;
		try {
			Integer price = Dao.getManagerCost();
			if (price <= coins) {
				Dao.modifyScore(idUser, (coins - price));
				e.setPoints(coins - price);
				Equipe equipe = e.getEquipe(e.getEquipesAvailable());
				for (Integer i = 0; i < e.getMineAvailable(); i++) {
					if (!e.getMine(i).hasTeam()) {
						e.getMine(i).hireTeam(equipe);
						bool = true;
						break;
					}
				}
				e.addEquipe();
			} else {
				System.out.println("addManager fail");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return bool;
	}

	/**
	 * Methode qui retourne l'id max dans la table des users
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static Integer maxId() throws SQLException {
		Integer maxId = -1;
		PreparedStatement ps = conn.prepareStatement("SELECT MAX(IdUser) FROM users;");
		ResultSet res = ps.executeQuery();
		res.next();
		try {
			maxId = res.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return maxId;
	}
}
