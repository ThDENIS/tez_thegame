package src;

/**
* Classe abstraite servant de base à tous les jeux, en prenant en compte son plateau
*/

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public abstract class Jeu implements Serializable{
	
	protected int largeur_plateau; 
	protected int longueur_plateau; 
	protected int[][] plateau; 
	
	public Jeu(int largeur_plateau, int longueur_plateau) {
		this.largeur_plateau = largeur_plateau;
		this.longueur_plateau = longueur_plateau; 
		this.plateau = new int[this.largeur_plateau][this.longueur_plateau];
		for (int i=0; i<this.largeur_plateau; i++) {
			for (int j=0; j<longueur_plateau; j++) {
				this.plateau[i][j] = 0; 
			}
		}
	}
	
	public String displayPlateau() {
		String s = ""; 
		return s; 
	}
	
	
}
