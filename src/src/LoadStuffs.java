package src;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/LoadStuffs")
public class LoadStuffs extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Non utilis�
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String isAuth = (String) session.getAttribute("authenticated");
		if (isAuth == "yes") {
			Entreprise e = (Entreprise) session.getAttribute("entreprise");
			session.setAttribute("entreprise", e);
			response.sendRedirect("http://localhost:8080/TEZ_BitcoinQuest/connected/ok.jsp");
		}
	}
}
