package src;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

/**
* Fonctionnalites du jeu TicTacToe
*/

public class TicTacToe extends Jeu implements Serializable{

	//private static final long serialVersionUID = 1L;
	private int tour_jeu; 
	
	
	public TicTacToe() {
		super(3, 3);
		tour_jeu = 1; 
	}
	
	public int getTourJeu() {
		return tour_jeu;
	}
	

	/**
	* Permet de changer le joueur qui doit jouer
	*/ 
	public void changeTourJeu(int tour) {
		tour_jeu = tour_jeu == 1 ? 2 : 1;
	}
	
	/**
	* Fonction qui permet d'afficher le plateau de jeu. La fonction onClick appelle une fonction du fichier tictactoe.js, qui permet de choisir la case sur laquelle on souhaite placer son pion
	*/
	public String displayPlateau() {
		String s = ""; 
		
		String pion = "";
		for (int i=0; i<this.longueur_plateau; i++) {
			for (int j=0; j<this.largeur_plateau; j++) {

				switch (this.plateau[i][j]) {
					case 0 : 
						s += "<img class='tictactoe' src='../images/vide.png' onClick='chooseCase("+ i + "," + j + ")'> ";
						break;
					case 1 : 
						s += "<img class='tictactoe' src='../images/rond.png' onClick='chooseCase("+ i + "," + j + ")'> ";
						break;
					case 2 : 
						s += "<img class='tictactoe' src='../images/croix.png' onClick='chooseCase("+ i + "," + j + ")'> ";
						break;
				}
				
			}
			s += "<br/>";
		}
		
		return s; 
	}
	
	/**
	* Fonctions qui permettent de donner le gagnant, et si la partie est terminée
	*/ 
	public int isOver() {
		return isOver(this.plateau); 
	}
	public int isOver(int[][] game) {
		for (int i=0; i<this.largeur_plateau; i++) {
			if (ligne(i, game) != 0)
				return ligne(i, game); 
			if (colonne(i, game) != 0)
				return colonne(i, game); 
		}
		if (diagonale(game) != 0)
			return diagonale(game); 
		if (diagonale_inverse(game) != 0)
			return diagonale_inverse(game);
		return completed(game); 
		
	}
	
	/**
	* Fonction qui permet de savoir si le plateau est complet
	*/ 
	public int completed(int[][] game) {
		for (int i=0; i<this.largeur_plateau;i++) {
			for (int j=0; j<this.longueur_plateau; j++) {
				if (this.plateau[i][j] == 0)
					return 0; 
			}
		}
		return -1;
	}
	/**
	* Fonction qui permet de savoir si une ligne est completee par un joueur (si un joueur a gagne sur cette ligne)
	*/
	public int ligne(int i, int[][] game) {
		int ligne = game[i][0];
		for (int j=1; j<this.largeur_plateau; j++) {
			if (game[i][j] != ligne)
				return 0; 
		}
		return ligne; 
	}
	
	/**
	* Fonction qui permet de savoir si un joueur a gagne sur cette colonne
	*/
	public int colonne(int i, int[][] game) {
		int colonne = game[0][i];
		for (int j=1; j<this.longueur_plateau; j++) {
			if (game[j][i] != colonne)
				return 0; 
		}
		return colonne; 
	}
	
	/**
	* Fonction qui permet de savoir si un joueur a gagne sur la diagonale
	*/
	public int diagonale(int[][] game) {
		int diago = game[0][0];
		for (int i=1; i<this.longueur_plateau; i++) {
			if (game[i][i] != diago)
				return 0; 
		}
		return diago; 
	}
	
	/**
	* Fonction qui permet de savoir si un joueur a gagne sur la diagonale inverse
	*/
	public int diagonale_inverse(int[][] game) {
		int diago = game[0][this.longueur_plateau-1];
		for (int i=1; i<this.largeur_plateau; i++) {
			if (game[i][this.longueur_plateau - (i+1)] != diago)
				return 0; 
		}
		return diago; 
	}
	
	/**
	* Fonction qui affiche un plateau sur lequel on ne peut plus ouer : le plateau fini et qui affiche le gagnant
	*/
	public String displayWinner(int winner) {
		String s = ""; 
		
		String pion = "";
		for (int i=0; i<this.longueur_plateau; i++) {
			for (int j=0; j<this.largeur_plateau; j++) {

				switch (this.plateau[i][j]) {
					case 0 : 
						s += "<img class='tictactoe' src='../images/vide.png'> ";
						break;
					case 1 : 
						s += "<img class='tictactoe' src='../images/rond.png'> ";
						break;
					case 2 : 
						s += "<img class='tictactoe' src='../images/croix.png'> ";
						break;
				}
			}
			s += "<br/>";
		}
		s += "<br/>";
		if (winner == -1)
			s += "Match nul !!! ";
		else
			s += "PLAYER " + winner + "  YOU WIN !! ";
		return s; 
	}
	
	/**
	* Fonction qui renvoie les coups possibles (les cases sur lesquelles on peut jouer)
	*/
	public ArrayList<Coords> coupsPossibles(int[][] game) {
		ArrayList<Coords> coupsPossibles = new ArrayList<Coords>(); 
		for (int i=0; i<this.largeur_plateau; i++) {
			for (int j=0; j<this.longueur_plateau; j++) {
				if (game[i][j] == 0)
					coupsPossibles.add(new Coords(i,j));
			}
		}
		return coupsPossibles; 
	}
	
	/**
	* Fonction qui permet de jouer contre l'ordi : choisit une case aléatoire dans les cases jouables
	*/ 
	public Coords niveauFacile() {
		
		ArrayList<Coords> cp = new ArrayList<Coords>(coupsPossibles(this.plateau)); 

		int random = (int)(Math.random() * (cp.size()));
		
		return cp.get(random);
	} 

	/**
	* Fonctions permettant de jouer avec un niveau plus difficile --> pas abouties 
	*/
	public int evaluateCoup(int[][] game) {
		
		int evaluate = 0; 
		for (int i=0; i<this.largeur_plateau; i++) {
			if (isPossibleLigne(i, game))
				evaluate++; 
			if (isPossibleColonne(i, game))
				evaluate++;
		}
		if (isPossibleDiagonale(game))
			evaluate++; 
		if (isPossibleDiagonaleInverse(game))
			evaluate++; 
		return evaluate; 
		
		
	}
	
	public boolean isPossibleLigne(int i, int[][] game) {
		for (int j=0; j<this.longueur_plateau; j++) {
			if (game[i][j] == 1)
				return false;
		}
		return true; 
	}
	
	public boolean isPossibleColonne(int i, int[][] game) {
		for (int j=0; j<this.longueur_plateau; j++) {
			if (game[j][i] == 1)
				return false;
		}
		return true; 
	}
	
	public boolean isPossibleDiagonale(int[][] game) {
		for (int j=0; j<this.longueur_plateau; j++) {
			if (game[j][j] == 1)
				return false;
		}
		return true; 
	}
	
	public boolean isPossibleDiagonaleInverse(int[][] game) {
		for (int j=0; j<this.longueur_plateau; j++) {
			if (game[j][this.longueur_plateau - (j+1)] == 1)
				return false;
		}
		return true; 
	}
	
	public boolean isTerminal(int[][] node) {
		if (isOver(node) == 0 )
			return false; 
		return true; 
	}
	
	public int minimax(int[][] node, int depth, boolean maximizingPlayer) {
		int value; 
		if (depth == 0 || isTerminal(node)) {
			return evaluateCoup(node);
		}
		if (maximizingPlayer) {
			value = -100000000; 
			for (Coords c : coupsPossibles(node))
				value = max(value, minimax(node(node, c), depth-1, false)); 
			return value; 
		}
		else {
			value = 100000000;
			for (Coords c : coupsPossibles(node))
				value = min(value, minimax(node(node,c), depth -1, true)); 
			return value; 
		}
	}
	
	public int[][] node(int[][] game, Coords c) {
		int[][] nouveauPlacement = new int[this.largeur_plateau][this.longueur_plateau]; 
		for (int i=0; i<this.largeur_plateau; i++) {
			for (int j=0; j<this.longueur_plateau; j++)
				nouveauPlacement[i][j] = game[i][j]; 
		}
		nouveauPlacement[c.getX()][c.getY()] = 2;
		return nouveauPlacement; 
	}
	
	public int min(int i, int j) {
		return (i<j ? i : j);
	}
	
	public int max(int i, int j) {
		return (i>j ? i : j);
	}
	
	
	
}

