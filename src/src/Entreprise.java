package src;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

public class Entreprise implements Serializable{ 

	private static final long serialVersionUID = 1L;

	private String id = null;
	private String nom = null;
	private double points = 0d;
	private double coefficiant = 0d;
	private Integer nbEmployeeDisponible = 0;
	private boolean keepRunning = true;
	private MyThread thread = new MyThread() {
		public void run() {
			while (keepRunning) {
				try {
					Thread.sleep(1000);
					points += 10 * (getCoefficiant());
					Dao.modifyScore("0", points);
				} catch (InterruptedException | SQLException e) {
					e.printStackTrace();
				}
			}
		}
	};
	private ArrayList<Mine> mines = new ArrayList<Mine>();
	private Integer mineAvailable = 0;
	private ArrayList<Equipe> equipes = new ArrayList<Equipe>();
	private Integer equipesAvailable = 0;
	private ArrayList<Jeu> jeux; 
	/**
	 * Constructeur par d�faut
	 * 
	 * @throws SQLException
	 */
	public Entreprise() throws SQLException {
		this((Dao.maxId() + 1), "noname", 0, 1d, 0, 0, 0);
	}

	/**
	 * Constructeur avec param�tre
	 * 
	 * @param nom
	 * @param points
	 * @param nbEmployeeDisponible
	 */
	public Entreprise(Integer id, String nom, Integer points, double coefficiant, Integer nbEmployeeDisponible, Integer mineAvailable, Integer equipesAvailable) {
		super();
		this.id = id.toString();
		this.nom = nom;
		this.points = points;
		this.coefficiant = coefficiant;
		this.nbEmployeeDisponible = nbEmployeeDisponible;
		this.mines.add(new Mine(1, 1, "mine 1"));
		this.mines.add(new Mine(1, 1, "mine 2"));
		this.mines.add(new Mine(1, 1, "mine 3"));
		this.mines.add(new Mine(1, 1, "mine 4"));
		this.mines.add(new Mine(1, 1, "mine 5"));
		this.mines.add(new Mine(1, 1, "mine 6"));
		this.equipes.add(new Equipe(0, 0, new Manager("manager1 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager2 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager3 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager4 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager5 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager6 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager7 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager8 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager9 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager10 ", 10)));
		this.equipes.add(new Equipe(0, 0, new Manager("manager11 ", 10)));
		this.mineAvailable = mineAvailable;
		this.equipesAvailable = equipesAvailable;
		this.jeux = new ArrayList<Jeu>(); 
		this.jeux.add(new TicTacToe());
		this.jeux.add(new Puissance4());
		thread.start();
	}

	/**
	 * Constructeur avec param�tre
	 * 
	 * @param nom
	 * @param points
	 * @param nbEmployeeDisponible
	 * @param mines
	 */
	/**
	 * public Entreprise(String nom, Integer points, double coefficiant, Integer
	 * nbEmployeeDisponible, ArrayList<Mine> mines) { this(nom, points, coefficiant,
	 * nbEmployeeDisponible); this.mines.addAll(mines); }
	 **/

	// Getter et setter

	public ArrayList<Jeu> getJeux() {
		return jeux;
	}

	public void setJeux(ArrayList<Jeu> jeux) {
		this.jeux = jeux;
	}

	public String getIdEntreprise() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPoints() {
		return (double) ((int) (points * 100)) / 100; // eviter les grands nombres a virgule
	}

	public void setPoints(double points) {
		this.points = points;
	}

	public Integer getNbEmployeeDisponible() {
		return nbEmployeeDisponible;
	}

	public void setNbEmployeeDisponible(Integer nbEmployeeDisponible) {
		this.nbEmployeeDisponible = nbEmployeeDisponible;
	}

	public void setCoefficiant(double coefficiant) {
		this.coefficiant = coefficiant;
	}

	public double getCoefficiant() {
		return coefficiant + (double) (nbEmployeeDisponible);/// 10;
	}

	/**
	 *  Stop la boucle infini du thread
	 */
	public void stopRunning() {
		keepRunning = false;
	}

	public Integer getTotalEmployee() {
		Integer totalEmployee = nbEmployeeDisponible;
		for (Mine mine : mines) {
			totalEmployee += mine.getEquipe().getNbEmployes();
		}
		return totalEmployee;
	}

	public Integer getMineAvailable() {
		return mineAvailable;
	}

	public void setMineAvailable(Integer mineAvailable) {
		this.mineAvailable = mineAvailable;
	}

	public void addMine() {
		mineAvailable++;
	}

	public Mine getMine(int i) {
		if (i <= mineAvailable)
			return mines.get(i);
		else
			return null;
	}

	public boolean allMineHaveManager() {
		System.out.println(mineAvailable);
		for (Integer i = 0; i < mineAvailable; i++)
			if (!mines.get(i).hasTeam())
				return false;
		return true;
	}
	
	public boolean allManagerIsHired() {
		if(equipesAvailable>5)
			return true;
		return false;
	}

	public Integer nbMineWithManager() {
		Integer nbMineWithManager = 0;
		for (Integer i = 0; i < mineAvailable; i++)
			if (mines.get(i).hasTeam())
				nbMineWithManager++;
		return nbMineWithManager;
	}

	public Integer getEquipesAvailable() {
		return equipesAvailable;
	}

	public void addEquipe() {
		equipesAvailable++;
	}

	public Equipe getEquipe(Integer i) {
		return equipes.get(i);
	}

	public Equipe getEquipe(String managerName) {
		for (Integer i = 0; i < equipesAvailable; i++)
			if (equipes.get(i).getManager().getNom().equals(managerName))
				return equipes.get(i);

		return null;
	}

	public boolean stillManaging(Manager m1) {
		for (Integer i = 0; i < mineAvailable; i++)
			if (mines.get(i).getEquipe() != null)
				if (m1 != null && m1.getNom().equals(mines.get(i).getEquipe().getManager().getNom()))
					return true;

		return false;
	}
	
	public void launchThread() {
		this.thread.start();
	}
	public void stopThread() {
		this.keepRunning = false;
	}

}
