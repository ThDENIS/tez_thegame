package src;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public abstract class EncryptPass {

	/**
	 * M�thodes qui prennent en param�tre un mot de passe et  retourne son SHA-256
	 * @param pass
	 * @return
	 */
		public static String toHash(String pass) {
			return encryptString(pass);
		}

		private static String encryptString(String password)
		{
		    String sha1 = "";
		    try
		    {
		        MessageDigest crypt = MessageDigest.getInstance("SHA-256");
		        crypt.reset();
		        crypt.update(password.getBytes("UTF-8"));
		        sha1 = byteToHex(crypt.digest());
		    }
		    catch(NoSuchAlgorithmException e)
		    {
		        e.printStackTrace();
		    }
		    catch(UnsupportedEncodingException e)
		    {
		        e.printStackTrace();
		    }
		    return sha1;
		}
		
		private static String byteToHex(final byte[] hash)
		{
		    Formatter formatter = new Formatter();
		    for (byte b : hash)
		    {
		        formatter.format("%02x", b);
		    }
		    String result = formatter.toString();
		    formatter.close();
		    return result;
		}
	}