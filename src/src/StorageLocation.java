package src;

/**
 * Classe contenant le lieu de stockage sur le serveur des serializations. Attention, la cr�ation fonctionne pour les localisations en 3 parties comme ci-dessous.
 * @author thiba
 *
 */
public final class StorageLocation {
	private static String storageLocation[] = {"C:\\", "Server\\","data"};
	
	public static String getStorageLocation(Integer index) {
		if(index<storageLocation.length)
			return storageLocation[index];
		else 
			return null;
	}
}
