package src;

import java.io.Serializable;

public class User implements Serializable{
	
	String login;
	String password;
	boolean isAdmin;
	
	
	User(){
		login = "";
		password = "";
		isAdmin = true;
	}
	
	public User(String login, String password, boolean isAdmin) {
		super();
		this.login = login;
		this.password = password;
		this.isAdmin = isAdmin;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
}
