package src;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Auth")
public class Auth extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * M�thode appel�e pour verifier l'existance d'un user dans la base de donn�es.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String login = request.getParameter("login");
		String pass = request.getParameter("password");
		RequestDispatcher disp;
		try {
			Integer idUser = Integer.parseInt(Dao.connect(login, EncryptPass.toHash(pass)));
			if (idUser > -1) {
				try { // Si l'user � un compte d�j� serialis�, on le r�cup�re
					FileInputStream fis = new FileInputStream(
							StorageLocation.getStorageLocation(0) + StorageLocation.getStorageLocation(1)
									+ StorageLocation.getStorageLocation(2) + "\\" + idUser + ".txt");
					ObjectInputStream ois = new ObjectInputStream(fis);
					Entreprise eTemp = (Entreprise) ois.readObject();
					Entreprise e = new Entreprise(Integer.parseInt(eTemp.getIdEntreprise()), eTemp.getNom(),
							(int) eTemp.getPoints(), eTemp.getCoefficiant(), eTemp.getNbEmployeeDisponible(),
							eTemp.getMineAvailable(), eTemp.getEquipesAvailable());
					session.setAttribute("authenticated", "yes");
					session.setAttribute("entreprise", e);
					response.sendRedirect(request.getContextPath() + "/connected/ok.jsp");
				} catch (Exception e1) { // sinon on en cr�� un nouveau
					e1.printStackTrace();
					Entreprise e = new Entreprise(idUser, login + "'s company", 0, 1, 0, 0, 0);
					session.setAttribute("authenticated", "yes");
					session.setAttribute("entreprise", e);
					response.sendRedirect(request.getContextPath() + "/connected/ok.jsp");
				}

			} else {
				session.setAttribute("authenticated", "non");
				disp = request.getRequestDispatcher("fail.jsp");
				disp.forward(request, response);
			}
		} catch (SQLException e2) {
			e2.printStackTrace();
		}

	}
}
