package src;

import java.io.Serializable;
import java.util.ArrayList;

public class Mine implements Serializable{

	private static final long serialVersionUID = 1L;
	private String nom = null;
	private Integer niveau = null;
	private Integer valeur = null;
	private Equipe equipe = null;
	
	/**
	 * Constructeur par d�faut 
	 */
	public Mine() {
		this(1,0,"mine 0", null);
	}
	
	/**
	 * Contructeur avec des param�tres
	 * @param niveau
	 * @param valeur
	 */
	public Mine(Integer niveau, Integer valeur, String nom) {
		this(niveau, valeur, nom, new Equipe(0,0));
	}
	
	/**
	 * Contructeur avec des param�tres
	 * @param niveau
	 * @param valeur
	 * @param employes
	 */
	public Mine(Integer niveau, Integer valeur, String nom, Equipe equipe) {
		super();
		this.niveau = niveau;
		this.valeur = valeur;
		this.nom = nom;
		this.equipe = equipe;
	}

	//Getter et setter
	
	public Integer getNiveau() {
		return niveau;
	}

	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}

	public Integer getValeur() {
		return valeur;
	}

	public void setValeur(Integer valeur) {
		this.valeur = valeur;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void hireTeam(Equipe equipe) {
		this.equipe = equipe;
	}
	
	public boolean fireTeam() {
		equipe = null;
		return true;
	}
	
	public boolean hasTeam() {
		if(equipe == null || equipe.getManager().getNom().equals("noname"))
			return false;
		return true;
	}
	
	
}
