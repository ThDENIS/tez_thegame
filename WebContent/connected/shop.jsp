<%@include file="includes/header.jsp"%>

<script>
	function majPrice() {
		document.getElementById('totalPrizeEmployee').innerHTML = dao
				.getEmployeeCost()
				+ getElementsByName("quantities");
	}
</script>

<div class="center">
	<h1>
		Bienvenue dans le Magasin
		<%=e.getNom()%>
		!<br>
	</h1>

	<h2>Que voulez vous faire?</h2>
	<form class="cadreShop" action="../AddEmployee" method="POST">
		<p>
			Acheter des employ�e (<%=dao.getEmployeeCost()%>
			l'unit�e)
		</p>
		<p>
			Vous avez
			<%=e.getTotalEmployee()%>
			employee(s)
		</p>
		<input type="number" name="quantities" value="1" onchange="majPrice()" />
		<p id="totalPrizeEmployee"></p>
		<input type="submit" value="Envoyer" />
	</form>

	<form class="cadreShop" action="../AddMine" method="POST">
		<p>
			Acheter des mines (<%=dao.getMineCost()%>
			l'unit�e)
		</p>
		<p id="totalPrizeEmployee"></p>
		<%
			if (e.getMineAvailable() > 5)
				out.println("<input type='submit' value='Envoyer' disabled='disabled'/>");
			else
				out.println("<input type='submit' value='Envoyer' />");
		%>
	</form>

	<form class="cadreShop" action="../AddManager" method="POST">
		<p>
			Acheter des managers (<%=dao.getManagerCost()%>
			l'unit�e)
		</p>
		<p id="totalPrizeEmployee"></p>
		<%
			if (e.allManagerIsHired())
				out.println("<input type='submit' value='Envoyer' disabled='disabled'/>");
			else
				out.println("<input type='submit' value='Envoyer' />");
		%>
	</form>
</div>

<%@include file="includes/footer.jsp"%>

