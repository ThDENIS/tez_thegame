<%@include file="includes/header.jsp"%>

		<div class="center">
			<h1>
				Bienvenue dans la vue globale des jeux 
				<%=e.getNom()%> !<br>
			</h1>
			
			<h2>TicTacToe</h2>
			<br/>

			<div id="board_tictactoe">
				<% out.println(e.getJeux().get(0).displayPlateau());%>
			</div>
		</div>
		
		<p style = "bottom: 0; position: fixed;">This application was created by : Zoe Chevallier, Etienne Guillou and Thibault Denis, </p>
	</body>
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	
	<script src = "../js/tictactoe.js"> 
	</script>
</html>