<%@ page import="src.Entreprise"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../style.css" type="text/css"
	media="screen" />
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<title>Bitcoin Quest</title>
</head>
<body onload="loadPoints()">

	<jsp:useBean id="dao" class="src.Dao" />
	<%
		Entreprise e = (Entreprise) session.getAttribute("entreprise");
		if (e == null) {
	%>
	<jsp:forward page="../fail.jsp" />
	<%
		}
	%>

	<script>
		score =
	<%=e.getPoints()%>
		;
		second = 0;
		coefficiant =
	<%=e.getCoefficiant()%>
		function incrementServer() {
			if (second > 59) {
				location.reload();	
				second = 0;
			}
			second = second + 1;
			setTimeout(incrementServer, 1000);
		}

		function incrementClient() {
			document.getElementById("score").innerHTML = "Points : "
					+ score.toFixed(2);
			if (document.getElementById("scoreCenter"))
				document.getElementById("scoreCenter").innerHTML = score
						.toFixed(2);
			score = score + 10 * coefficiant;
			setTimeout(incrementClient, 1000);
		}

		function loadPoints() {
			incrementClient();
			incrementServer();
		}
	</script>

	<header>
	<ul class="niveau1">
		<li><img src="../images/menu.png"
			onclick="window.location.href = 'ok.jsp'" alt="Menu logo"
			class="menuLogo">
			<ul class="niveau2">
				<li class="inMenu"><a href="shop.jsp">Aller magasiner</a></li>
				<li class="inMenu"><a href="mines.jsp">Aller voir les mines</a></li>
				<li class="inMenu"><a href="manager.jsp">Aller voir les
						managers</a></li>
				<li class="inMenu"><a href="jeux.jsp">Aller voir les
				jeux</a></li>
				<li class="inMenu"><a href="ok.jsp">retourner a l'accueil</a></li>
			</ul></li>
	</ul>
	<div id="score" class="center">Points :</div>
	<a href="disconnectPage.jsp" class="right affichageHeader">se
		deconnecter</a>
	<br>
	</header>