-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 07, 2018 at 12:02 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bitcoin_quest`
--

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE IF NOT EXISTS `shop` (
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`name`, `price`) VALUES
('employee', 10),
('mine', 100),
('manager', 25);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `IdUser` int(255) NOT NULL AUTO_INCREMENT,
  `Login` char(255) NOT NULL,
  `Hash` char(255) NOT NULL,
  `score` double NOT NULL,
  `IsAdmin` binary(1) DEFAULT NULL,
  PRIMARY KEY (`IdUser`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`IdUser`, `Login`, `Hash`, `score`, `IsAdmin`) VALUES
(5, 'td', '30fc9e3356c21227177859ff28b144526d9bcaf9802b1bc19ed5709ca482f8ae', 10160, 0x01),
(2, 'thibault', 'ca75819cada8553113205db8175de9b6e1d634181efae7e6c690d61ef7a65cbc', 410, 0x01),
(3, 'zoe', '9d017e2681b7f31725e1c0fbe2612e89079c22806b02cf7a894b500dd5a219c1', 10120, 0x00),
(4, 'etienne', '1a30ed961f81b1d86bad52343c93e738d8844095491d3f2d7bbc022ccd6b9512', 970, 0x00),
(6, 'jabril', '8e57d7b28129ce669e2956bb9d9ae2ee9f1443ce7a5549f0a330d9939df15881', 540, 0x30);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
